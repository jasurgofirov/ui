import React ,{useState} from "react";
import './footer.css';
import { Modal } from "./Modal";

     function Footer(){
                // /Modal //
                const [showModal, setShowModal] = useState(false)

                const openModal =()=>{
                setShowModal(prev=>!prev)
                }
                // /Modal///

            

    return(
      
        <div className="footer_container">

            <div className="footer_container_1">
           
                <h1 className="footer_h1_yordam">Bepul konsultatsiya yordami uchun</h1>

                <input type="text" placeholder="Ismingiz" className="footer_input" />
                <input type="tel" placeholder="Telefon raqamingiz"  className="footer_input"/>
                <button className="footer_btn" onClick={openModal}>Konsultatsiya olish</button>
                <Modal showModal={showModal} setShowModal={setShowModal}></Modal>


            </div>

            <div className="footer_container_2">
                <div className="ishVaqti">
                <img src="./image/vector.svg" alt="telegram" />
                <p className="ishvaqtiP">Ish vaqti</p>
                </div>
                <p className="ish_kunlariP">Ish kunlari 10:00 dan  22:00 gacha
                <br />
                 Dam olish kunlarisiz
                </p>
                <div className="footerImg_wrapper">
                    <img className="footerImgIcon1" src="./image/telegram.svg" />
                    <img className="footerImgIcon2" src="./image/call.svg" />
                    <img className="footerImgIcon3" src="./image/instagramm.svg" />

            </div>
            </div>
            <div className="footer_container_3">
                <p className="footer_adress">Internet-market.uz <br />
                +998(99)911-02-04      <br />
                Pahlavon Mahmud ko'chasi, <br />
                Yashnabod tumani, <br />
                Toshkent
                </p>
                <p className="footer_adress">Разработано в Support Solutions <br /> Все права защищены.</p>
            </div>



        </div>

    )
}
export default Footer;