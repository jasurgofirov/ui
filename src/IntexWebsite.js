import './App.css';
import Header from './header/header'
import Main from './main/main'
import Footer from './footer/footer'
import Section from './Section-1/Section-1';


function IntexWebsite() {
  return (
      <>
        <Header/>
        <Main />
        <Section/>
        <Footer />  
       </>  
  );
}

export default IntexWebsite;
